/**
 * @name kr01
 *
 * @desc Proje kapsamı: KR 01

 * AEGON_RAPOR1.xlsx (Aegon-Ödeme Aksatan Poliçeler Exceli örneği)
 * AEGON_RAPOR2.xlsx (AEGON Üretim Exceli örneği)
 * AEGON_RAPOR3.xlsx (Aegon-Acente Komisyon Raporu örneği)
 * ALLIANZ_RAPOR1.xlsx (Allianz-Tahsil Edilmemiş Katkı Payı Vadeleri örneği)
 * ALLIANZ_RAPOR2.xlsx (ALLIANZ Üretim Exceli okunur)

Adım 7: Bu excelde aşağıdaki sütunlar silinir:
  SATIŞ_KANALI, BOLGE_GRUBU, BOLGE, SUBE, SALESMANAGERNAMESURNAME,
  SALESMANAGERPARTAJ, EY_ACENTE_ADI, FINANSAL_DANISMAN, KAMPANYA_KODU, ODEME_DONEMI
Adım 8: En başa “Danışman” adında yeni bir sütun eklenir.
Adım 9: AEGON Üretim Exceli okunur. Bir örneği AEGON_RAPOR2 olarak bu dosyaya ek olarak konulmuştur. Bu excelin ilk sütunu “Danışman Adı”, ikinci sütunu “Müşteri Adı”dır.
Adım 10: AEGON_RAPOR1’deki (Aegon-Ödeme Aksatan Poliçeler Exceli) MUSTERI_ADSOYAD sütunundaki her isim, ile AEGON_RAPOR2’deki (Aegon Üretim Excel’i) ikinci sütun (Müşteri Adı) karşılaştırılır. Eşleşme olduğu zaman, AEGON_RAPOR2’deki ilk sütun (Danışman Adı), AEGON_RAPOR1’deki 8. Adımda oluşturulmuş “Danışman” sütununa yazılır.
  Özetle, ödeme aksatan müşteriler excelindeki müşterilerin, hangi danışmana ait olduğu üretim raporunda aratılıp, eşleşme olunca, ödeme aksatan müşteri exceline yazılmaktadır.
  İsimler normal şartlar altında “kimlikte yazıldığı gibi” yazılmaktadır, fakat ı,i harfleri, büyük, küçük harf gibi konularda uyuşmamazlık olabilir. Alternatif senaryolar zaman içinde görüldükçe robota eklenecektir.
  Bu adım AEGON_RAPOR1 (Aegon-Ödeme Aksatan Poliçeler) raporundaki tüm müşteriler için eşleştirme yapılıncaya ve “Danışman” sütununa yazılıncaya kadar devam eder.
Adım 11: Eşleştirme yapılmış toplu excel (Aegon-Ödeme Aksatan Poliçeler), İstiklal Hanım’a mail atılır. (istiklal@istiklalsigortacilik.com.tr)
Adım 12: Excel her “Danışman” için ayrı ayrı parçalara ayrılır. Parçalanmış exceller her danışmana ayrı ayrı mail atılır.
Adım 13: Allianz’ın AZ-NET sistemine giriş yapılır (cas.allianz.com.tr/cas/login).
Adım 14: Üretim > BES > Raporlar sayfasına gidilir.
Adım 15: 121-Tahsil Edilmemiş Katkı Payı Vadeleri raporu açılır (üretim kategorisi altında).
Adım 16: “EXCEL Önizleme” butonuna tıklanır. Rapor otomatik olarak iner.
  Bu raporun bir örneği ALLIANZ_RAPOR1 (Allianz-Tahsil Edilmemiş Katkı Payı Vadeleri) örnek olarak ekte bulunmaktadır.
Adım 17: Bu excelde boş sütunlar ve “KatılımcıSicil No” sütunları silinir.
Adım 18: “SözleşmeBaşlangıçTarihi” son 2 yıl içinde olmayan kayıtlar silinir. Örneğin Aralık 2019’da isek, Aralık 2017 öncesine dair kayıtlar silinir. Aralık 2017 kaydı silinmez.
Adım 19: Mavi olan kayıtlar “aslında ödenmiş ama provizyonu tamamlanmadığı için” bu raporda olan kayıtlardır. Bunlar silinir.
Adım 20: Eğer Ö.A.A. (Ödenmemiş Ay Adeti) eğer 1’den fazla ise aşağı doğru satırlar çoklanmaktadır. Çoklanmış sütunlar tekilleştirilmelidir (tarih ve miktar dışındaki sütunlar silinebilir).
Adım 21: Ö.A.A.’sı 1’den fazla olanlar için AZ-NET ekranında tekrar sorgulama yapılması gerekmektedir. Bu kayıtların “Bes Hesap No” bilgisi üzerinden sorgulama yapılacaktır.
Adım 22: (Ö.A.A.’sı 1’den fazla olanlar için) AZ-NET ekranından İşlemler > Üretim > BES > Sözleşme Görüntüle tıklanır.
Adım 23: (Ö.A.A.’sı 1’den fazla olanlar için) AZ-NET ekranından İşlemler > Üretim > BES > Sözleşme Görüntüle tıklanır.
Adım 24: (Ö.A.A.’sı 1’den fazla olanlar için) Bes No alanına, excelden alınan “BES Hesap No” alanındaki numara yazılır ve “Ara” butonuna tıklanır.
Adım 25: (Ö.A.A.’sı 1’den fazla olanlar için). Arama yapıldıktan sonra aşağıda sonuç gelir ve “Görüntüleme” butonuna tıklanır.
Adım 26: (Ö.A.A.’sı 1’den fazla olanlar için). Gelen ekranda “Ödeme Planı” tıklanır.
Adım 27: (Ö.A.A.’sı 1’den fazla olanlar için). “Provizyon Alınan Katkı Payı” 0 olan aylar ödenmemiş aylardır.
  Beklenen Ödeme Tarihi, 1) bugünden önce olan, 2) ödenmemiş ayların tarihlerin bilgisi buradan alınır.
Adım 28: (Ö.A.A.’sı 1’den fazla olanlar için). AZ-NET’den alınan “ödenmemiş” ödeme tarihleri bilgisi ALLIANZ_RAPOR1’deki “Vade Tarihi” sütununa yanyana (ve topluca) yazılır.
Adım 29: “Danışman” sütunu eklenir.
Adım 30: ALLIANZ Üretim Exceli okunur. Bir örneği ALLIANZ_RAPOR2 olarak bu dosyaya ek olarak konulmuştur. Bu excelin ilk sütunu “Danışman Adı”, ikinci sütunu “Müşteri Adı”dır.
Adım 31: ALLIANZ_RAPOR1’deki (Allianz-Tahsil Edilmemiş Katkı Payı Vadeleri) “Katılımcı Adı Soyadı” sütunundaki her isim, ile ALLIANZ_RAPOR2’deki (Allianz Üretim Excel’i) ikinci sütun (Müşteri Adı) karşılaştırılır. Eşleşme olduğu zaman, ALLIANZ_RAPOR2’deki ilk sütun (Danışman Adı), ALLIANZ_RAPOR1’deki 29. Adımda oluşturulmuş “Danışman” sütununa yazılır.
  Özetle, ödeme aksatan müşteriler excelindeki müşterilerin, hangi danışmana ait olduğu üretim raporunda aratılıp, eşleşme olunca, ödeme aksatan müşteri exceline yazılmaktadır.
  İsimler normal şartlar altında “kimlikte yazıldığı gibi” yazılmaktadır, fakat ı,i harfleri, büyük, küçük harf gibi konularda uyuşmamazlık olabilir. Alternatif senaryolar değerlendirilmeli ve ona göre eşleştirme yapılmalıdır.
  Bu adım ALLIANZ_RAPOR1 (Allianz-Tahsil Edilmemiş Katkı Payı Vadeleri) raporundaki tüm müşteriler için eşleştirme yapılıncaya ve “Danışman” sütununa yazılıncaya kadar devam eder.
Adım 32: Eşleştirme yapılmış toplu excel (Allianz-Tahsil Edilmemiş Katkı Payı Vadeleri), İstiklal Hanım’a mail atılır. (istiklal@istiklalsigortacilik.com.tr)
Adım 33: Excel her “Danışman” için ayrı ayrı parçalara ayrılır. Parçalanmış exceller her danışmana ayrı ayrı mail atılır.

Süreç sonlanır.

 */
"use strict";

/***********
MODULES
************/
const XLSX=require('xlsx');
const fs = require('fs')
const nodemailer = require('nodemailer');
const moment = require('moment');
var pyshell = require('python-shell');

/***********
DEFINITIONS
************/

// excel related items
var EXCEL_PATH;
var EXCEL_RESULTS;
var EXCEL_AEGON_DELAYED
var EXCEL_AEGON_PRODUCTION;
var EXCEL_AEGON_AGENCY;
var EXCEL_ALLIANZ_UNCOLLECTED;
var EXCEL_ALLIANZ_PRODUCTION;
var sheetarray_aegon_delay;
var sheetarray_aegon_production;
var sheetarray_aegon_agency;
var sheetarray_allianz_uncollected;
var sheetarray_allianz_production;
var sheetarray_result;
var workbook_allianz_uncollected;
var consultant_excels_aegon = [];
var consultants_aegon = [];
var consultant_excels_allianz = [];
var consultants_allianz = [];

// mail informations
const MAIL_FROM = 'ertanuysal@gmail.com'
const MAIL_PASSWORD = 'xxxxxxxxxx'
const MAIL_ISTIKLAL = 'ertanuysal@gmail.com'
const MAIL_DANISMAN_GLOBAL = 'ertanuysal@gmail.com'

// external python program name
var PYTHON_PROGRAM = "/projects/freelances/mert/kronnikaexcel/kronnikaExcel.py";

// debug level
var debug_level = 1;

// Sheet to array
var sheet2arr = function(sheet){
    var result = [];
    var row;
    var rowNum;
    var colNum;
    var range = XLSX.utils.decode_range(sheet['!ref']);
    for(rowNum = range.s.r; rowNum <= range.e.r; rowNum++){
       row = [];
        for(colNum=range.s.c; colNum<=range.e.c; colNum++){
           var nextCell = sheet[
              XLSX.utils.encode_cell({r: rowNum, c: colNum})
           ];
           if( typeof nextCell === 'undefined' ){
              row.push(void 0);
           } else row.push(nextCell.w);
        }
        result.push(row);
    }
    return result;
 };

// Array to sheet
var arr2sheet = function(sheetarray, excel_name)
{
  var ws_name = "Sheet";
  var wb = XLSX.utils.book_new(), ws = XLSX.utils.aoa_to_sheet(sheetarray);
  /* add worksheet to workbook */
  XLSX.utils.book_append_sheet(wb, ws, ws_name);
  /* write workbook */
  try {
    fs.unlinkSync(EXCEL_RESULTS + excel_name)
  }
  catch(error) {
  }

  fs.mkdirSync(EXCEL_RESULTS, { recursive: true });
  XLSX.writeFile(wb, EXCEL_RESULTS + excel_name);
}

// Convert turkish to english for step 10
String.prototype.turkishtoEnglish = function () {
  return this.replace(/Ğ/gim, "g")
    .replace(/Ü/gim, "u")
    .replace(/Ş/gim, "s")
    .replace(/I/gim, "i")
    .replace(/İ/gim, "i")
    .replace(/Ö/gim, "o")
    .replace(/Ç/gim, "c")
    .replace(/ğ/gim, "g")
    .replace(/ü/gim, "u")
    .replace(/ş/gim, "s")
    .replace(/ı/gim, "i")
    .replace(/ö/gim, "o")
    .replace(/ç/gim, "c");
};

// send mail with attached file
async function mail_sender(mail_to, mail_subject, attached_file_1, attached_file_2)
{
   var mail = nodemailer.createTransport({
   service: 'gmail',
       auth: {
       user: MAIL_FROM,
       pass: MAIL_PASSWORD
   }
   });

   var mailOptions = {
       from: MAIL_FROM,
       to: mail_to,
       subject: mail_subject
   }

  // attach the second one if it is not empty
  if (attached_file_2){
    mailOptions.attachments = [{path: EXCEL_RESULTS + attached_file_1, filename: attached_file_1},
                              {path: EXCEL_RESULTS + attached_file_2, filename: attached_file_2}];
  }
  else {
    mailOptions.attachments = [{path: EXCEL_RESULTS + attached_file_1, filename: attached_file_1}];
  }

  mail.sendMail(mailOptions, function(error, info){
    if (error) {
      logger("Email could not be sent", "error");
    } else {
      logger('Email sent: ' + info.response, "info");
    }
  });
}

// Initialize all needed globals
// Adım 30: ALLIANZ Üretim Exceli okunur. Bir örneği ALLIANZ_RAPOR2 olarak bu dosyaya ek olarak konulmuştur.
async function init_all()
{
  logger("read all excel files to the seperated arrays at a time", "info");

  var args = process.argv.slice(2);
  if(args[1] == "linux") {
    EXCEL_PATH = process.cwd()+"/excels/"
    EXCEL_RESULTS = EXCEL_PATH + "results/"
    EXCEL_AEGON_DELAYED = EXCEL_PATH + 'AEGON_RAPOR1.xlsx';
    EXCEL_AEGON_PRODUCTION = EXCEL_PATH + 'AEGON_RAPOR2.xlsx';
    EXCEL_AEGON_AGENCY = EXCEL_PATH + 'AEGON_RAPOR3.xlsx';
    EXCEL_ALLIANZ_UNCOLLECTED = EXCEL_PATH + 'ALLIANZ_RAPOR1.xlsx';
    EXCEL_ALLIANZ_PRODUCTION = EXCEL_PATH + 'ALLIANZ_RAPOR2.xlsx';
  }
  else
  {
    EXCEL_PATH = process.cwd()+"\\excels\\"
    EXCEL_RESULTS = EXCEL_PATH + "results\\"
    EXCEL_AEGON_DELAYED = EXCEL_PATH + 'AEGON_RAPOR1.xlsx';
    EXCEL_AEGON_PRODUCTION = EXCEL_PATH + 'AEGON_RAPOR2.xlsx';
    EXCEL_AEGON_AGENCY = EXCEL_PATH + 'AEGON_RAPOR3.xlsx';
    EXCEL_ALLIANZ_UNCOLLECTED = EXCEL_PATH + 'ALLIANZ_RAPOR1.xlsx';
    EXCEL_ALLIANZ_PRODUCTION = EXCEL_PATH + 'ALLIANZ_RAPOR2.xlsx';
  }

  const workbook_aegon_delay=XLSX.readFile(EXCEL_AEGON_DELAYED);
  const workbook_aegon_production=XLSX.readFile(EXCEL_AEGON_PRODUCTION);
  const workbook_aegon_agency=XLSX.readFile(EXCEL_AEGON_AGENCY);
  workbook_allianz_uncollected=XLSX.readFile(EXCEL_ALLIANZ_UNCOLLECTED, {cellStyles:true});
  const workbook_allianz_production=XLSX.readFile(EXCEL_ALLIANZ_PRODUCTION);

  sheetarray_aegon_delay = sheet2arr(workbook_aegon_delay.Sheets[workbook_aegon_delay.SheetNames[0]]);
  sheetarray_aegon_production = sheet2arr(workbook_aegon_production.Sheets[workbook_aegon_production.SheetNames[0]]);
  sheetarray_aegon_agency = sheet2arr(workbook_aegon_agency.Sheets[workbook_aegon_agency.SheetNames[0]]);
  sheetarray_allianz_uncollected = sheet2arr(workbook_allianz_uncollected.Sheets[workbook_allianz_uncollected.SheetNames[0]]);
  sheetarray_allianz_production = sheet2arr(workbook_allianz_production.Sheets[workbook_allianz_production.SheetNames[0]]);
}

// debug enable/disable from command line
async function init_debug()
{
  var args = process.argv.slice(2);
  switch(args[0]) {
    case "debug"  : debug_level = 3; break;
    case "info"   : debug_level = 2; break;
    case "error"  : debug_level = 1; break;
    default       : debug_level = 1;
  }

  logger("Debug level was set to: " + debug_level, "error" );
}

// Consol debug log
function logger(extra_log, loglevel)
{
  var tmp_level;

  var color_red    = '\x1b[31m%s\x1b[0m';
  var color_cyan   = '\x1b[36m%s\x1b[0m';
  var color_yellow = '\x1b[33m%s\x1b[0m';
  var color_log;

  switch(loglevel) {
    case "debug"  : tmp_level = 3; color_log = color_yellow; break;
    case "info"   : tmp_level = 2; color_log = color_cyan; break;
    case "error"  : tmp_level = 1; color_log = color_red; break;
    default       : tmp_level = 1;
  }

  if(debug_level >= tmp_level) {
    console.log(color_log, extra_log);
  }
}

/*
Adım 7: Bu excelde aşağıdaki sütunlar silinir:
  SATIŞ_KANALI, BOLGE_GRUBU, BOLGE, SUBE, SALESMANAGERNAMESURNAME,
  SALESMANAGERPARTAJ, EY_ACENTE_ADI, FINANSAL_DANISMAN, KAMPANYA_KODU, ODEME_DONEMI
*/
async function step_7()
{
  logger("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Enter step_7", "debug");

  logger("\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ AEGON started", "info");
  logger("Remove no needed columns", "info");

  sheetarray_result = sheetarray_aegon_delay;

  for (let i = 0; i < sheetarray_result.length; i++) {
    sheetarray_result[i].splice(0, 8);
    sheetarray_result[i].splice(4, 1);
    sheetarray_result[i].splice(9, 1);
  }
}

// Adım 8: En başa “Danışman” adında yeni bir sütun eklenir.
async function step_8()
{
  logger("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Enter step_8", "debug");

  logger("Add a new column named Danışman", "info");

  sheetarray_result[0].unshift("Danışman");
  for (let i = 1; i < sheetarray_result.length; i++) {
    sheetarray_result[i].unshift(void 0);
  }
}

/*
Adım 10: AEGON_RAPOR1’deki (Aegon-Ödeme Aksatan Poliçeler Exceli) MUSTERI_ADSOYAD sütunundaki her isim, ile AEGON_RAPOR2’deki (Aegon Üretim Excel’i) ikinci sütun (Müşteri Adı) karşılaştırılır. Eşleşme olduğu zaman, AEGON_RAPOR2’deki ilk sütun (Danışman Adı), AEGON_RAPOR1’deki 8. Adımda oluşturulmuş “Danışman” sütununa yazılır.
  Özetle, ödeme aksatan müşteriler excelindeki müşterilerin, hangi danışmana ait olduğu üretim raporunda aratılıp, eşleşme olunca, ödeme aksatan müşteri exceline yazılmaktadır.
  İsimler normal şartlar altında “kimlikte yazıldığı gibi” yazılmaktadır, fakat ı,i harfleri, büyük, küçük harf gibi konularda uyuşmamazlık olabilir. Alternatif senaryolar zaman içinde görüldükçe robota eklenecektir.
  Bu adım AEGON_RAPOR1 (Aegon-Ödeme Aksatan Poliçeler) raporundaki tüm müşteriler için eşleştirme yapılıncaya ve “Danışman” sütununa yazılıncaya kadar devam eder.
*/
async function step_10()
{
  logger("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Enter step_10", "debug");

  logger("Fil column->Danışman", "info");

  var turkish_result, turkish_aegon;
  var found_consultant;
  for (let i = 1; i < sheetarray_result.length; i++) {
    found_consultant = 0;
    for (let j = 0; j < sheetarray_aegon_production.length; j++) {
      try {
        turkish_result = sheetarray_result[i][1].turkishtoEnglish();
        turkish_aegon = sheetarray_aegon_production[j][1].turkishtoEnglish();
        logger(turkish_result.toLowerCase() + "==" +  turkish_aegon.toLowerCase(), "debug");
        if(turkish_result.toLowerCase() == turkish_aegon.toLowerCase()) {
          logger("overdue payment: " + sheetarray_result[i][1], "debug");
          sheetarray_result[i][0] = sheetarray_aegon_production[j][0]
          found_consultant = 1;
          continue;
        }
      }
      catch(error) {
        continue
      }
    }
    if(found_consultant == 0) {
      logger("not found: " + sheetarray_result[i][1], "error");
      sheetarray_result[i][0] = "danışman eşleşemedi";
    }
  }

  arr2sheet(sheetarray_result, "result.xlsx");
  logger("result.xlsx has been saved", "info");
}

//Adım 12: Excel her “Danışman” için ayrı ayrı parçalara ayrılır.
async function step_12()
{
  logger("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Enter step_12", "debug");

  logger("Split the result.xls for each consultant", "info");
  /* Fill a new array with the consultants (remove empty lines, duplicates, just the consultants */
  var searched_consultant = [];
  var k = 0;
  var is_consultant_taken;
  for (let i = 1; i < sheetarray_result.length; i++) {
    if(sheetarray_result[i][0]){
      is_consultant_taken = 0;
      for (let j = 0; j < searched_consultant.length; j++) {
        if(sheetarray_result[i][0] == searched_consultant[j]) {
          is_consultant_taken = 1;
          break;
        }
      }
      if(!is_consultant_taken && sheetarray_result[i][0] != "danışman eşleşemedi") {
        searched_consultant[k] = sheetarray_result[i][0];
        k++;
      }
    }
  }
  logger("All found consultants", "debug");
  logger(searched_consultant, "debug");;

  /* Search for each consultant */
  var tmp_consultant_array = [];
  for (let i = 0; i < searched_consultant.length; i++) {
    tmp_consultant_array = []
    tmp_consultant_array[0] = sheetarray_result[0];
    k = 1;
    for (let j = 1; j < sheetarray_result.length; j++) {
      if(searched_consultant[i] == sheetarray_result[j][0]) {
        tmp_consultant_array[k] = sheetarray_result[j];
        k++;
      }
    }
    var consultant_excel_name = "result_" + searched_consultant[i].split(" ").join("_") + ".xlsx";
    logger("Created new excel (Consultant: " + searched_consultant[i] + " File: " + consultant_excel_name + ")", "debug");
    consultant_excels_aegon[i] = consultant_excel_name;
    consultants_aegon[i] = searched_consultant[i];
    arr2sheet(tmp_consultant_array, consultant_excel_name);
  }
}

/*
Adım 17: Bu excelde boş sütunlar ve “KatılımcıSicil No” sütunları silinir.
*/
async function step_17()
{
  logger("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Enter step_17", "debug");

  logger("\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ALLIANZ started", "info");
  sheetarray_result = sheetarray_allianz_uncollected;

  logger("Do some corrections in the excel", "info");
  /* correction: Vade Tarihi is in a wrong cell */
  sheetarray_result[1][20] = sheetarray_result[2][20];
  sheetarray_result[2][20] = undefined;

  /* correction: KatılımcıSicil No is in a wrong cell */
  sheetarray_result[1][0] = sheetarray_result[1][1]
  sheetarray_result[1][1] = undefined;

  /* correction: ToplamBorç is in a wrong cell */
  sheetarray_result[1][17] = sheetarray_result[1][18];
  sheetarray_result[1][18] = undefined;

  /* remove empty columns */
  logger("Remove empty columns", "info");
  var column_count = sheetarray_result[1].length;
  /* first define a 2 dimensional array */
  var tmp_sheetarray_result = [];
  for (var i = 0; i < sheetarray_result.length; i++ ) {
    tmp_sheetarray_result[i] = [];
  }

  /* second remove the full empty columns */
  var k = 0;
  for (let i = 0; i < column_count; i++) {
    if(sheetarray_result[1][i] != undefined) {
      for (let j = 0; j < sheetarray_result.length; j++) {
        tmp_sheetarray_result[j][k] = sheetarray_result[j][i];
        logger("i: " + i + " j: " + j + " k = " + k, "debug");
        logger("tmp_sheetarray_result[j][i] = " + sheetarray_result[j][i], "debug");
      }
      k++;
    }
  }
  sheetarray_result = tmp_sheetarray_result;

  /* remove KatılımcıSicil No column */
  logger("Remove KatılımcıSicil No column", "info");
  for (let i = 0; i < sheetarray_result.length; i++) {
    sheetarray_result[i].splice(0, 1);
  }

  // save the result as an excel if debug is enabled
  if(debug_level == 3) {
    arr2sheet(sheetarray_result, "step_17.xlsx");
  }
}

/* Adım 19: Mavi olan kayıtlar “aslında ödenmiş ama provizyonu tamamlanmadığı için” bu raporda olan kayıtlardır. Bunlar silinir. */
async function step_19()
{
  logger("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Enter step_19", "debug");

  logger("Remove all blue rows", "info");

  /* Prepare a tmp array which will be filled by the step 18 result */
  var tmp_sheetarray_result = [];
  tmp_sheetarray_result[0] = sheetarray_result[0];
  tmp_sheetarray_result[1] = sheetarray_result[1];
  tmp_sheetarray_result[2] = sheetarray_result[2];
  var k = 3;
  var nextCell;
  for (let i = 3; i < sheetarray_result.length; i++) {
    try {
      /* Find the blue colors */
      nextCell = workbook_allianz_uncollected.Sheets[workbook_allianz_uncollected.SheetNames[0]][XLSX.utils.encode_cell({r: i, c: 23})];
      if(nextCell.s.fgColor.rgb == 'C4E2FE') {
        logger('Skip: it is blue', "debug");
        continue;
      }
      else{
        logger('Do no skip: it is not blue', "debug");
        tmp_sheetarray_result[k] = sheetarray_result[i];
        k++;
      }
    }
    catch(error) {
      continue;
    }
  }
  sheetarray_result = tmp_sheetarray_result;

  // save the result as an excel if debug is enabled
  if(debug_level == 3) {
    arr2sheet(sheetarray_result, "step_19.xlsx");
  }
}

/* Adım 18: “SözleşmeBaşlangıçTarihi” son 2 yıl içinde olmayan kayıtlar silinir. Örneğin Aralık 2019’da isek, Aralık 2017 öncesine dair kayıtlar silinir. Aralık 2017 kaydı silinmez. */
async function step_18()
{
  logger("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Enter step_18", "debug");

  logger("Remove records which are more than 2 years old", "info");

  /* Prepare dates */
  var olddate, olddate_formatted;

  var suanki_zaman;
  const MONTH_AMOUNT = 24;
  suanki_zaman = moment();
  olddate = moment(suanki_zaman).subtract(MONTH_AMOUNT, 'months');

  olddate_formatted = moment(suanki_zaman).subtract(MONTH_AMOUNT, 'months').format("MM/DD/YYYY");
  var old_date = new Date(olddate_formatted);
  var sheetarray_result_date;

  /* Prepare a tmp array which will be filled by the step 19 result */
  var tmp_sheetarray_result = [];
  tmp_sheetarray_result[0] = sheetarray_result[1];
  var k = 1;
  for (let i = 3; i < sheetarray_result.length; i++) {
    if(sheetarray_result[i][3]) {
      sheetarray_result_date = new Date(sheetarray_result[i][3]);
      if(sheetarray_result_date < old_date) {
        logger('Skip: sheetarray_result_date < old_date', "debug");
        continue;
      }
      else{
        logger('Do not skip: sheetarray_result_date >= old_date', "debug");
        tmp_sheetarray_result[k] = sheetarray_result[i];
        k++;
      }
    }
    else {
        logger('Do not Skip empty dates they will be needed for step 20', "debug");
        tmp_sheetarray_result[k] = sheetarray_result[i];
        k++;
    }
  }
  sheetarray_result = tmp_sheetarray_result;

  // save the result as an excel if debug is enabled
  if(debug_level == 3) {
    arr2sheet(sheetarray_result, "step_18.xlsx");
  }
}

// Adım 20: Eğer Ö.A.A. (Ödenmemiş Ay Adeti) eğer 1’den fazla ise aşağı doğru satırlar çoklanmaktadır. Çoklanmış sütunlar tekilleştirilmelidir (tarih ve miktar dışındaki sütunlar silinebilir).
async function step_20()
{
  logger("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Enter step_20", "debug");

  logger("Multiplexed columns must be singulated", "info");

  /* Prepare a tmp array which will be filled by the step 20 result */
  var tmp_sheetarray_result = [];
  tmp_sheetarray_result[0] = sheetarray_result[0];
  var k = 1;
  var current_oaa;
  for (let i = 1; i < sheetarray_result.length; i++) {
    current_oaa = sheetarray_result[i][6];
    if(current_oaa == undefined) {
      logger('Skip. Ö.A.A. = ' + current_oaa, "debug");
    }
    else if(current_oaa == 1) {
      logger('Skip process but copy it to the new excel. Ö.A.A. = ' + current_oaa, "debug");
      tmp_sheetarray_result[k] = sheetarray_result[i];
      k++;
    }
    else {
      logger('Do not skip: Ö.A.A. = '+ current_oaa, "debug");
      tmp_sheetarray_result[k] = sheetarray_result[i];
      var m = 1;
      while (sheetarray_result[i + m][6] == undefined) {
        logger("sheetarray_result[i + m][6] : " + sheetarray_result[i + m][6], "debug");
        logger("sheetarray_result[i + m][8] : " + sheetarray_result[i + m][8], "debug");
        logger("sheetarray_result[i + m][9] : " + sheetarray_result[i + m][9], "debug");
        logger("sheetarray_result[i + m][10] : " + sheetarray_result[i + m][10], "debug");
        tmp_sheetarray_result[k][8] += "\n" + sheetarray_result[i + m][8];
        tmp_sheetarray_result[k][9]  += "\n" +  sheetarray_result[i + m][9];
        tmp_sheetarray_result[k][10] += "\n" + sheetarray_result[i + m][10];
        m++;
      }
      i = i + m;
      k++;
    }
  }
  sheetarray_result = tmp_sheetarray_result;

  // save the result as an excel if debug is enabled
  if(debug_level == 3) {
    arr2sheet(sheetarray_result, "step_20.xlsx");
  }
}

// Adım 29: “Danışman” sütunu eklenir.
async function step_29()
{
  logger("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Enter step_29", "debug");

  logger("Add a new column named Danışman ", "info");

  sheetarray_result[0].unshift("Danışman");
  for (let i = 1; i < sheetarray_result.length; i++) {
    sheetarray_result[i].unshift(void 0);
  }

  // save the result as an excel if debug is enabled
  if(debug_level == 3) {
    arr2sheet(sheetarray_result, "step_29.xlsx");
  }
}

/*
Adım 31: ALLIANZ_RAPOR1’deki (Allianz-Tahsil Edilmemiş Katkı Payı Vadeleri) “Katılımcı Adı Soyadı” sütunundaki her isim, ile ALLIANZ_RAPOR2’deki (Allianz Üretim Excel’i) ikinci sütun (Müşteri Adı) karşılaştırılır. Eşleşme olduğu zaman, ALLIANZ_RAPOR2’deki ilk sütun (Danışman Adı), ALLIANZ_RAPOR1’deki 29. Adımda oluşturulmuş “Danışman” sütununa yazılır.
  Özetle, ödeme aksatan müşteriler excelindeki müşterilerin, hangi danışmana ait olduğu üretim raporunda aratılıp, eşleşme olunca, ödeme aksatan müşteri exceline yazılmaktadır.
  İsimler normal şartlar altında “kimlikte yazıldığı gibi” yazılmaktadır, fakat ı,i harfleri, büyük, küçük harf gibi konularda uyuşmamazlık olabilir. Alternatif senaryolar değerlendirilmeli ve ona göre eşleştirme yapılmalıdır.
  Bu adım ALLIANZ_RAPOR1 (Allianz-Tahsil Edilmemiş Katkı Payı Vadeleri) raporundaki tüm müşteriler için eşleştirme yapılıncaya ve “Danışman” sütununa yazılıncaya kadar devam eder.
*/
async function step_31()
{
  logger("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Enter step_31", "debug");

  var turkish_result, turkish_production;

  logger("Fil column->Danışman ", "info");

  var found_consultant;
  for (let i = 1; i < sheetarray_result.length; i++) {
    found_consultant = 0;
    for (let j = 0; j < sheetarray_aegon_production.length; j++) {
      try {
        turkish_result = sheetarray_result[i][2].turkishtoEnglish();
        turkish_production = sheetarray_allianz_production[j][1].turkishtoEnglish();
        logger(turkish_result.toLowerCase() + "==" +  turkish_production.toLowerCase(), "debug");
        if(turkish_result.toLowerCase() == turkish_production.toLowerCase()) {
          sheetarray_result[i][0] = sheetarray_allianz_production[j][1];
          found_consultant = 1;
          continue;
        }
      }
      catch(error) {
        continue
      }
    }
    if(found_consultant == 0) {
      logger("not found: " + sheetarray_result[i][2], "error");
      sheetarray_result[i][0] = "danışman eşleşemedi";
    }
  }

  arr2sheet(sheetarray_result, "result_allianz.xlsx");
  logger("result_allianz.xlsx has been saved", "debug");
}

// Adım 33: Excel her “Danışman” için ayrı ayrı parçalara ayrılır.
async function step_33()
{
  logger("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Enter step_33", "debug");

  logger("Split the result.xls for each consultant", "info");

  /* Fill a new array with the consultants (remove empty lines, duplicates, just the consultants */
  var searched_consultant = [];
  var k = 0;
  var is_consultant_taken;
  for (let i = 1; i < sheetarray_result.length; i++) {
    if(sheetarray_result[i][0]){
      is_consultant_taken = 0;
      for (let j = 0; j < searched_consultant.length; j++) {
        if(sheetarray_result[i][0] == searched_consultant[j]) {
          is_consultant_taken = 1;
          break;
        }
      }
      if(!is_consultant_taken && sheetarray_result[i][0] != "danışman eşleşemedi") {
        searched_consultant[k] = sheetarray_result[i][0];
        k++;
      }
    }
  }
  logger("All found consultants", "debug");
  logger(searched_consultant, "debug");

  /* Search for each consultant */
  var tmp_consultant_array = [];
  for (let i = 0; i < searched_consultant.length; i++) {
    tmp_consultant_array[0] = sheetarray_result[0];
    k = 1;
    for (let j = 1; j < sheetarray_result.length; j++) {
      if(searched_consultant[i] == sheetarray_result[j][0]) {
        tmp_consultant_array[k] = sheetarray_result[j];
        k++;
      }
    }
    var consultant_excel_name = "result_allianz" + searched_consultant[i].split(" ").join("_") + ".xlsx";
    logger("Created new excel (Consultant: " + searched_consultant[i] + " File: " + consultant_excel_name + ")", "debug");
    consultant_excels_allianz[i] = consultant_excel_name;
    consultants_allianz[i] = searched_consultant[i];
    arr2sheet(tmp_consultant_array, consultant_excel_name);
  }
}

// convert non formatted excel to kronnika format via a pyhton app
async function convert_kronnika_and_mail()
{
  var options = {
    args: [EXCEL_RESULTS]
  };
  pyshell.PythonShell.run(PYTHON_PROGRAM, options, function  (err, results)  {
    if  (err)  throw err;
    logger(PYTHON_PROGRAM + ' finished with ' + results, "info");
    send_mails();
  });
}

// Adım 11: aegon: Eşleştirme yapılmış toplu excel (Aegon-Ödeme Aksatan Poliçeler), İstiklal Hanım’a mail atılır. (istiklal@istiklalsigortacilik.com.tr)
// Adım 12: aegon: Parçalanmış exceller her danışmana ayrı ayrı mail atılır.
// Adım 32: allianz: Eşleştirme yapılmış toplu excel (Allianz-Tahsil Edilmemiş Katkı Payı Vadeleri), İstiklal Hanım’a mail atılır. (istiklal@istiklalsigortacilik.com.tr)
// Adım 33: allianz: Parçalanmış exceller her danışmana ayrı ayrı mail atılır.
async function send_mails()
{
  // MAILs: AEGON
  mail_sender(MAIL_ISTIKLAL, 'Results: Ms. İstiklal', 'result.xlsx', '');

  for (let i = 1; i < consultants_aegon.length; i++) {
    mail_sender(MAIL_DANISMAN_GLOBAL, 'Results: ' + consultants_aegon[i].split(" "), consultant_excels_aegon[i], '');
  }

  // MAILs: ALLIANZ
  logger("Allianz mail: Send the result_allianz.xls to Ms. İstiklal ", "info");
  mail_sender(MAIL_ISTIKLAL, 'Results: Ms. İstiklal', 'result_allianz.xlsx', '');
  for (let i = 1; i < consultants_allianz.length; i++) {
    mail_sender(MAIL_DANISMAN_GLOBAL, 'Results Allianz: ' + consultants_allianz[i].split(" "), consultant_excels_allianz[i], '');
  }
}

init_all();
init_debug();
step_7();
step_8();
// Skip Adım 9 (init_all içerisinde yapıldı)
step_10();
step_12();
// Skip [Adım 13-Adım 16]
step_17();
// MUST: Run step 19 before step 18
step_19();
step_18();
step_20();
// TODO: Step [21, 28]
step_29();
// Skip Adım 30 (init_all içerisinde yapıldı)
step_31();
step_33();
convert_kronnika_and_mail();